from features.lib.pages.locators.side_bar_locator import SIDE_BAR_LOCATORS as L
from features.lib.pages.common.base_page import BasePage


class SideBarPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.driver, '')
        self.context = context
        self.l_gestion_pm = None

    def gestion_pm(self):
        self.l_gestion_pm = super().find_ec_visibility(*L['GESTION_PM'])
        self.l_gestion_pm.click()


