import os
from features.lib.pages.locators.login_locators import LOGIN_LOCATORS as L
from features.lib.pages.common.base_page import BasePage


class LoginPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.driver, context.config.userdata['url'] + L['URL'] )
        super().open()
        self.context = context
        self.input_cuil = None
        self.input_pss = None
        self.btn_confirm = None

    def login(self, type_usr):

        if type_usr == 'SUPERADMIN':
            usr = os.getenv('cuilSuperAdmin')
            pwd = os.getenv('passSuperAdmin')
        elif type_usr is None:
            pass

        self.input_cuil = super().find_ec_visibility(*L['USR'])
        self.input_pss = super().find_ec_visibility(*L['PASS'])
        self.btn_confirm = super().find_ec_visibility(*L['CONFIRM'])

        self.input_cuil.send_keys(usr)
        self.input_pss.send_keys(pwd)
        self.btn_confirm.click()





