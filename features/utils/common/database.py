import cx_Oracle
import os

from dotenv import load_dotenv


class DB:

    def __init__(self):
        load_dotenv()
        CONN_INFO = {
            'host': os.getenv('host'),
            'port': os.getenv('port'),
            'user': os.getenv('user'),
            'psw': os.getenv('psw'),
            'service': os.getenv('service')
        }
        self.CONN_STR = '{user}/{psw}@{host}:{port}/{service}'.format( **CONN_INFO )

    def open(self):
        #agregar try para manejar falla
        try:
            self.conn = cx_Oracle.connect( self.CONN_STR )
        except Exception as err:
            print('Falló la conexión a la base '+ str(err))

    def query(self, query, params=None):
        cursor = self.conn.cursor()
        result = cursor.execute( query, params ).fetchall()
        cursor.close()
        return result

    def close(self):
        self.conn.close()
