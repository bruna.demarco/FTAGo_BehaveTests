from features.utils.common.database import DB


class GestionPMDB:

    def __init__(self):
        self.query = None
        self.params = None
        self.database = DB()

    def consultar_cookie_por_cuit(self, cuit):
        self.database.open()
        self.query = 'select SJ.COOKIE from SOJ_USER SJ where SJ.CUIT = :cuil'
        self.params = {'cuil': cuit}
        result = self.database.query(self.query, self.params)
        self.database.close()
        return result
