setuptools==40.8.0
requests==2.22.0
behave==1.2.6
selenium==3.141.0

python-dotenv~=0.15.0