from time import sleep

from behave import *
from hamcrest import *
from features.lib.pages.side_bar_page import SideBarPage
from features.lib.pages.gestion_pm_page import GestionPMPage

@given('abro la pagina de gestion PM')
def abrir_gestion_pm(context):
    side_bar_page = SideBarPage(context)
    side_bar_page.gestion_pm()

@given('abro las opciones del primer regsitro de la tabla')
def primer_registro_op(context):
    gestion_pm_page = GestionPMPage(context)
    gestion_pm_page.click_btn_opciones_1()
    context.gestion_pm_page = gestion_pm_page

@given('selecciono eliminar cookies')
def click_borrar_cookie(context):
    context.gestion_pm_page.click_borrar_cookie()

@then('verifico que las cookies se eliminaron')
def verificar_borrado_cookie(context):
    cuil = context.gestion_pm_page.obtener_cuil_1()
    result = context.gestion_pm_page.verifiar_cookie_eliminada(cuil)
    assert_that(result[0][0], equal_to(None))
