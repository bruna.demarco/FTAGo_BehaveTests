from features.lib.pages.locators.gestion_pm_locators import GESTION_PM_LOCATORS as L
from features.lib.pages.common.base_page import BasePage
from features.utils.gestion_pm_db import GestionPMDB


class GestionPMPage(BasePage):

    def __init__(self, context):
        BasePage.__init__(self, context.driver, '')
        self.context = context
        self.btn_opciones_1 = None
        self.borrar_cookie = None
        self.cuil_1 = None
        self.db = GestionPMDB()

    def click_btn_opciones_1(self):
        self.btn_opciones_1 = super().find_ec_visibility(*L['REGISTRO_1'])
        self.btn_opciones_1.click()

    def click_borrar_cookie(self):
        self.borrar_cookie = super().find_ec_visibility(*L['BORRAR_COOKIE'])
        self.borrar_cookie.click()

    def obtener_cuil_1(self):
        self.cuil_1 = super().find_ec_visibility(*L['CUIT_1'])
        return self.cuil_1.text.replace('-', '')

    def verifiar_cookie_eliminada(self, cuil):
        return self.db.consultar_cookie_por_cuit(cuil)


