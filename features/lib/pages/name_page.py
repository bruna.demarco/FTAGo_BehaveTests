__author__='Tomas Rios'
from features.lib.locators import Locator
from selenium.webdriver.common.by import By


class NamePage(object):

    def __init__(self):
        """
        #Name Page Locators
        self.lnk_howAreWe = driver.driver.find_element(By.ID, Locator.element)
        """
        #Name Page Methods
    def clickLink(self):
        self.lnk_howAreWe.click()

    def find_element_by_id(self, element_id):
        return self.driver.find_element_by_id(element_id)
