from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage(object):

    def __init__(self, driver, base_url, timeout=10):
        self.driver = driver
        self.base_url = base_url
        self.driver_wait = WebDriverWait(self.driver, timeout)

    def open(self):
        self.driver.get( self.base_url )

    def find_ec_visibility(self, *term):
        self.wait_until_angular()
        return self.driver_wait.until(EC.visibility_of_element_located((term[0], term[1])))

    def get_url(self):
        return self.driver.current_url

    def wait_until_angular(self):
        java_script_to_load_angular = """
        callback = arguments[arguments.length - 1];
        try {
            var testabilities = window.getAllAngularTestabilities();
            var count = testabilities.length;
            var decrement = function() {
            count--;
            if (count === 0) {
              callback('completed');
                }
            };
            testabilities.forEach(function(testability) {
                testability.whenStable(decrement);
            });
         } catch (err) {
            callback(err.message);
         }
        """
        return self.driver.execute_script(java_script_to_load_angular)


