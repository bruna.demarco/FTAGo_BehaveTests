from selenium.webdriver.common.by import By

LOGIN_LOCATORS = \
    {'USR': (By.XPATH, '*//input[@id="user"]'),
     'PASS': (By.XPATH, '*//input[@id="password"]'),
     'CONFIRM': (By.XPATH, '*//button[@id="login"]'),
     'URL': 'loginInterno'}
