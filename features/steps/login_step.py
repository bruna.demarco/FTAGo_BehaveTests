from behave import *
from hamcrest import *
from features.lib.pages.login_page import LoginPage

@given('me logueo como "{type_usr}"')
def login(context, type_usr):
    login = LoginPage(context)
    login.login(type_usr)

